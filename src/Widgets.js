import React from 'react';
import './assets/css/Widgets.css';
import { TwitterTimelineEmbed, TwitterTweetEmbed } from 'react-twitter-embed';
import SearchIcon from '@mui/icons-material/Search';

function Widgets() {
    return (
        <div className="widgets section">
            <div className="search input field">
                <SearchIcon className="search icon"/>
                <input placeholder="Search Twitter" type="text"/>
            </div>

            <div className="container">
                <h3>What's happening</h3>

                <TwitterTimelineEmbed sourceName="profile" screenName="s_thrall" options={{height:400}}/>
            </div>
        </div>
    )
}

export default Widgets;