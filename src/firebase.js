import { initializeApp } from "firebase/app";
import { getFirestore } from 'firebase/firestore';

const firebaseConfig = {
  apiKey: "AIzaSyAcalZZ-BajFNqKIKd_DHy0t7u3JULOHWM",
  authDomain: "microblog-4a76b.firebaseapp.com",
  projectId: "microblog-4a76b",
  storageBucket: "microblog-4a76b.appspot.com",
  messagingSenderId: "445528015079",
  appId: "1:445528015079:web:3b2ef767055d81ec4c2242",
  measurementId: "G-78TVNX32C8"
};

const app = initializeApp(firebaseConfig);
const db = getFirestore(app);

export default db;