import React from 'react';
import './assets/css/SidebarOption.css'

function SidebarOption({ active, text, Icon }) {
    return (
        <div className={`sidebar option ${active && 'active'}`}>
            <Icon/>
            <span className="text">{ text }</span>
        </div>
              
    );
}

export default SidebarOption;