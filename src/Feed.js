import React, { useEffect, useState } from 'react';
import { query, orderBy, collection, getDocs } from 'firebase/firestore';
import PostBox from './PostBox.js';
import Post from './Post.js';
import './assets/css/Feed.css';
import db from './firebase.js';
import FlipMove from 'react-flip-move';

function Feed() {
    const [posts, setPosts] = useState([]);

    useEffect(() => {
        getDocs(collection(db, "posts")).then((snapshot) => {
            snapshot.forEach((doc) => {
                setPosts(snapshot.docs.map(doc => doc.data()).sort((a,b) => { return b.timestamp - a.timestamp }));
            });
        });
    }, []);

    return (
        <div className="feed section">
            <div className="header">
                <h3>Home</h3>
            </div>

            <PostBox/>

            <FlipMove>
                {posts.map(post => (
                <Post
                    key={post.text}
                    displayName = { post.displayName }
                    username = { post.username }
                    verified = { post.verified }
                    text = { post.text }
                    image = { post.image }
                    avatar = { post.avatar }
                />
                ))}
            </FlipMove>
        </div>
    )
}

export default Feed;