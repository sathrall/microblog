import React, { ref, forwardRef } from 'react';
import './assets/css/Post.css';
import { Avatar } from "@mui/material";
import VerifiedIcon from '@mui/icons-material/Verified';
import ChatBubbleOutlineIcon from '@mui/icons-material/ChatBubbleOutline';
import RepeatIcon from '@mui/icons-material/Repeat';
import FavoriteBorderIcon from '@mui/icons-material/FavoriteBorder';
import PublishIcon from '@mui/icons-material/Publish';

const Post = forwardRef(
    ({ displayName, username, verified, text, image, avatar }) => {
    return (
        <div className="post panel" ref={ref}>
            <div className="avatar">
                <Avatar src={ avatar }/>
            </div>
            <div className="body">
                <div className="header">
                    <h3>{ displayName }</h3>
                    { verified && <VerifiedIcon className="badge"/> }
                    <h3>@{ username }</h3>
                </div>
                <div className="body">
                    <p>{ text }</p>
                    { image && <img src={ image } alt="post"/> }
                </div>
            </div>
            <div className="footer">
                <ChatBubbleOutlineIcon fontSize="small"/>
                <RepeatIcon fontSize="small"/>
                <FavoriteBorderIcon fontSize="small"/>
                <PublishIcon fontSize="small"/>
            </div>
        </div>
    )
});

export default Post;