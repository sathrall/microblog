import React, { useState } from 'react';
import { Avatar, Button } from "@mui/material";
import './assets/css/PostBox.css';
import db from './firebase.js';
import { doc, setDoc, Timestamp } from 'firebase/firestore';

function PostBox() {
    const [postMessage, setPostMessage] = useState('');
    const [postImage, setPostImage] = useState('');

    const sendPost = e => {
        e.preventDefault();

        const postData = {
            displayName: 'Scott Thrall',
            username: 's_thrall',
            verified: true,
            timestamp: Timestamp.fromDate(new Date()),
            text: postMessage,
            image: postImage,
            avatar: 'https://via.placeholder.com/150'
        };

        setDoc(doc(db, 'posts', makeId(16)), postData);

        setPostMessage("");
        setPostImage("");
    }

    const makeId = (length) => {
        var result           = '';
        var characters       = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
        var charactersLength = characters.length;
        for ( var i = 0; i < length; i++ ) {
            result += characters.charAt(Math.floor(Math.random() * 
        charactersLength));
        }
        return result;
    }

    return (
        <div className="post box">
            <form>
                <div className="input field">
                    <Avatar src="https://via.placeholder.com/150" />
                    <input placeholder="What's happening?" type="text" value={postMessage} onChange={(e) => setPostMessage(e.target.value)} />
                </div>
                <input className="image input" placeholder="Optional: Enter image URL" type="text" value={postImage} onChange={(e) => setPostImage(e.target.value)} />
                <Button className="post button" type="submit" onClick={sendPost}>Post</Button>
            </form>
        </div>
    )
}

export default PostBox;