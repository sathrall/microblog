import React from 'react';
import SidebarOption from './SidebarOption.js'
import './assets/css/Sidebar.css';
import DataObject from '@mui/icons-material/DataObject';
import HomeIcon from '@mui/icons-material/Home';
import SearchIcon from '@mui/icons-material/Search';
import NotificationsIcon from '@mui/icons-material/Notifications';
import EmailIcon from '@mui/icons-material/Email';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import DescriptionIcon from '@mui/icons-material/Description';
import PersonIcon from '@mui/icons-material/Person';
import PendingIcon from '@mui/icons-material/Pending';
import { Button } from '@mui/material'

function Sidebar() {
    return (
        <div className="sidebar section">
            <DataObject className="data icon"/>

            <SidebarOption active Icon={HomeIcon} text="Home"/>
            <SidebarOption Icon={SearchIcon} text="Explore"/>
            <SidebarOption Icon={NotificationsIcon} text="Notifications"/>
            <SidebarOption Icon={EmailIcon} text="Messages"/>
            <SidebarOption Icon={BookmarkIcon} text="Bookmarks"/>
            <SidebarOption Icon={DescriptionIcon} text="Lists"/>
            <SidebarOption Icon={PersonIcon} text="Profile"/>
            <SidebarOption Icon={PendingIcon} text="More"/>

            <Button variant="outlined" className="post button" fullWidth>Post</Button>
        </div>
    );
}

export default Sidebar;